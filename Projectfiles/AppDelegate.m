/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen ;;;. 
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

#import "AppDelegate.h"
#import "SceneOne.h"
@implementation AppDelegate

-(void) initializationComplete
{
#ifdef KK_ARC_ENABLED
	CCLOG(@"ARC is enabled");
#else
	CCLOG(@"ARC is either not available or not enabled");
#endif
}

-(id) alternateView
{
	return nil;
}
-(void)applicationDidEnterBackground:(UIApplication *)application
{
//    backgroundID = [application beginBackgroundTaskWithExpirationHandler:^{}];
//    SceneOne *sceneOne =  [[SceneOne alloc]initWithNumber:3];
//    [[CCDirector sharedDirector]replaceScene:sceneOne];
    [super applicationDidEnterBackground:application];
}
-(void)applicationDidBecomeActive:(UIApplication *)application
{
    [super applicationDidBecomeActive:application];
//    [application endBackgroundTask:backgroundID];
}
@end
