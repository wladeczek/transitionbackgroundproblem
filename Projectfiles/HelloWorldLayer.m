/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim. 
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

#import "HelloWorldLayer.h"
#import "SceneOne.h"
@interface HelloWorldLayer (PrivateMethods)
@end

@implementation HelloWorldLayer

-(id) init
{
	if ((self = [super init]))
	{
		number = 0;
        CCMenuItem *menuItem = [[CCMenuItemFont alloc]initWithString:[NSString stringWithFormat:@"transition to %d",number] target:self selector:@selector(transition)];
        CCMenu *menu = [CCMenu menuWithArray:@[menuItem]];
        menu.position = [[CCDirector sharedDirector]screenCenter];
        [self addChild:menu];
	}

	return self;
}
-(id)initWithNumber:(int)number_
{
    if((self = [super init])){
        number = number_;
        NSString *title = [NSString stringWithFormat:@"transition to %d",number];
        CCMenuItem *menuItem = [[CCMenuItemFont alloc]initWithString:title target:self selector:@selector(transition)];
        CCMenu *menu = [CCMenu menuWithArray:@[menuItem]];
        menu.position = [[CCDirector sharedDirector]screenCenter];
        [self addChild:menu];
    }
    return self;
}

-(void)transition
{
    int destinationNumber = ++number;
    SceneOne *destScene = [[SceneOne alloc]initWithNumber:destinationNumber];
    CCTransitionFade *transition = [[CCTransitionFade alloc]initWithDuration:2 scene:destScene];
    [[CCDirector sharedDirector]replaceScene:transition];
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [[CCDirector sharedDirector]replaceScene:[[SceneOne alloc]initWithNumber:++number]];
    });
    
}
-(void) dealloc
{
	// uncomment if you're not using ARC (ahem, make that: *still* not using ARC ...)
	//[super dealloc];
	
	// if you suspect a memory leak, put a breakpoint here to see if the node gets deallocated
	NSLog(@"dealloc: %@ %d", self,number);
}
@end
