//
//  SceneOne.m
//  My-Empty-Project-Project
//
//  Created by Wladyslaw Surala on 9/1/13.
//
//

#import "SceneOne.h"
#import "HelloWorldLayer.h"
@implementation SceneOne
-(id)initWithNumber:(int)number_
{
    self = [super init];
    if(self){
        number = number_;
    }
    return self;
}
-(void) onEnter
{
	[super onEnter];
    glClearColor(0.2f, 0.1f, 0.3f, 1.0f);
    HelloWorldLayer *layer = [[HelloWorldLayer alloc]initWithNumber:number];
    [self addChild:layer];
	// add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
}

-(void) cleanup
{
	[super cleanup];
    
	// any cleanup code goes here
	
	// specifically release/nil any references that could cause retain cycles
	// since dealloc might not be called if this class retains another node that is
    // either a sibling or in a different branch of the node hierarchy
}

-(void) dealloc
{
	// uncomment if you're not using ARC (ahem, make that: *still* not using ARC ...)
	//[super dealloc];
	
	// if you suspect a memory leak, put a breakpoint here to see if the node gets deallocated
	NSLog(@"dealloc: %@ %d", self,number);
}
@end
